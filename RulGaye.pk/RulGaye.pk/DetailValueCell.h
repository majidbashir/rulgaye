//
//  DetailValueCell.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 28/01/2016.
//  Copyright © 2016 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailValueCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titlleOfLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailOfLabel;

@end
