//
//  aboutCell.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 18/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface aboutCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ItemLabel;
@property (strong, nonatomic) IBOutlet UISwitch *userStateSwitch;

@end
