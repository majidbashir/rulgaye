//
//  PostAdViewController.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 26/01/2016.
//  Copyright © 2016 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationHandler.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"



static CGFloat kLZAlbumCreateVCPhotoSize = 65;

@interface PostAdViewController :NavigationHandler<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>


@property (nonatomic,strong) NSMutableArray * selectPhotos;


@property (weak, nonatomic) IBOutlet UICollectionView * photoCollectionView;
- (IBAction)PostAdTouchEvent:(id)sender;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField    *   titleField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView     *   descriptionTextView;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField    *   amountTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField    *   cityTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField    *   contactTextField;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextField    *   emailTextField;
- (IBAction)Terms:(id)sender;

@end
