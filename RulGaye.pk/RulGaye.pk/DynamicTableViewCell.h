//
//  DynamicTableViewCell.h
//  DynamicCellHeight
//
//  Created by Timo Josten on 08/07/15.
//  Copyright (c) 2015 mkswap.net. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DynamicTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *  titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *  description_Label;

@end
