//
//  postAd_Cell.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 08/12/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JJMaterialTextField.h"


@interface postAd_Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet JJMaterialTextfield * titleField;


/*
 
 @"India",@"country",
 @"Punjab",@"state",
 @"Rawalpindi",@"city",
 @"nowhere",@"Location",
 @"+923327223222",@"contact_phone",
 @"Majid Bashir",@"contact_name",
 @"majidbshir@gmail.com",@"contact_email",
 @"Hello this is something",@"description",
 nil];
 
 */

@end
