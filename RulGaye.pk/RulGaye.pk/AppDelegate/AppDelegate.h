//
//  AppDelegate.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 03/09/2015.
//  Copyright (c) 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Headers.h"
#import "CategoryManager.h"

// VC's imported to show on tabbar

#import "PostAdViewController.h"
#import "HomeVC.h"
#import "BrowseVC.h"
#import "MyFavouritesVC.h"
#import "AboutVC.h"
#import "PostAdViewController.h"



@interface AppDelegate : UIResponder <UIApplicationDelegate,UITabBarControllerDelegate>

@property (strong, nonatomic) UIWindow *window;




@end

