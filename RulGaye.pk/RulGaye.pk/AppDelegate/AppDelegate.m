//
//  AppDelegate.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 03/09/2015.
//  Copyright (c) 2015 Cozmuler. All rights reserved.
//

#import "AppDelegate.h"
#import "ZATabBarController.h"
#import <AFNetworking/AFNetworking.h>
#import "LoginScreen.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


// List The View Controller's Images
- (NSArray *)tabBarImages
{
    NSDictionary *imgDict1 = @{
                               @"Normal":  [UIImage imageNamed:@"home_tab"],
                               @"Active":  [UIImage imageNamed:@"home_tab_selected"]
                               };
    
    NSDictionary *imgDict2 = @{
                               @"Normal":  [UIImage imageNamed:@"user_tab"],
                               @"Active":  [UIImage imageNamed:@"user_tab_selected"]
                               };
    
    NSDictionary *imgDict3 = @{
                               @"Normal":  [UIImage imageNamed:@"post_ad_tab"],
                               @"Active":  [UIImage imageNamed:@"post_ad_tab_selected"]
                            };
    
    
    NSDictionary *imgDict4 = @{
                               @"Normal":  [UIImage imageNamed:@"favourite_tab"],
                               @"Active":  [UIImage imageNamed:@"favourite_tab_selected"]
                               };
    
    NSDictionary *imgDict5 = @{
                               @"Normal":  [UIImage imageNamed:@"setting_tab"],
                               @"Active":  [UIImage imageNamed:@"setting_tab_selected"]
                               };
    
    return @[imgDict1, imgDict2, imgDict3,imgDict4,imgDict5];
}



// List The View Controllers

- (NSArray *)tabBarControllers
{

    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    HomeVC * vc1 = [storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
    UINavigationController * nc1 = [[UINavigationController alloc] initWithRootViewController:vc1];
    
    
    BrowseVC * vc2 = [storyboard instantiateViewControllerWithIdentifier:@"BrowseVC"];
    UINavigationController * nc2 = [[UINavigationController alloc] initWithRootViewController:vc2];
    
    
    PostAdViewController * vc3 = [storyboard instantiateViewControllerWithIdentifier:@"PostAdViewController"];
    UINavigationController * nc3 = [[UINavigationController alloc] initWithRootViewController:vc3];
    
    
    MyFavouritesVC * vc4 = [storyboard instantiateViewControllerWithIdentifier:@"MyFavouritesVC"];
    UINavigationController * nc4 = [[UINavigationController alloc] initWithRootViewController:vc4];

    
    AboutVC * vc5 = [storyboard instantiateViewControllerWithIdentifier:@"AboutVC"];
    UINavigationController * nc5 = [[UINavigationController alloc] initWithRootViewController:vc5];
    
    
    NSMutableArray *tabVCArray = [NSMutableArray arrayWithObjects:nc1, nc2, nc3,nc4 , nc5, nil];
    
    return tabVCArray;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    

    dispatch_async (dispatch_get_main_queue(),
                    ^{
                        [self fetchCategories];
                    });

    

    NSArray * tabVCArray = [self tabBarControllers];
    NSArray * imgArray = [self tabBarImages];
  
    ZATabBarController *tabBarController = [[ZATabBarController alloc] init];
    [tabBarController setViewControllers:tabVCArray imageArray:imgArray];
    tabBarController.tabBar.buttonItemWidth = self.window.frame.size.width/5;
//    
   tabBarController.tabBar.backgroundView.backgroundColor = [UIColor colorWithRed:246/255.0f green:246/255.0f blue:246/255.0f alpha:1];
//
    tabBarController.tabBar.buttonInsets = UIEdgeInsetsMake(2, 5, 2, 2);
//    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
//
    self.window.rootViewController = tabBarController;
    [self.window makeKeyAndVisible];

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) fetchCategories
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes=[manager.responseSerializer.
                                                           acceptableContentTypes setByAddingObject:@"text/html"];
        [manager GET:(kBaseUrls @"getAllCategories") parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject)
         {
             CategoryManager * categorySharedInstance = [CategoryManager sharedManager];
             categorySharedInstance.cat_dict = responseObject ;
             
             NSLog(@"%@",categorySharedInstance.cat_dict);
         }
         
             failure:^(NSURLSessionTask *operation, NSError *error)
        {
             NSLog(@"Error %@",[[error userInfo]valueForKey:@"NSLocalizedDescription"]);
            [self fetchCategories];
        }];

}

@end
