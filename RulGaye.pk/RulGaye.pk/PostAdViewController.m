//
//  PostAdViewController.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 26/01/2016.
//  Copyright © 2016 Cozmuler. All rights reserved.
//

#import "PostAdViewController.h"
#import "LZAlbumPhotoCollectionViewCell.h"
#import "XHPhotographyHelper.h"


static NSString* photoCellIndentifier=@"cell";

@interface PostAdViewController ()

@property (strong,nonatomic) XHPhotographyHelper* photographyHelper;


@end

@implementation PostAdViewController
@synthesize photoCollectionView;

-(void)viewDidLoad
{
    [super viewDidLoad];

    
    self.title = @"POST AD";
    
    [self setUpElements];
    
    [self.photoCollectionView registerClass:[LZAlbumPhotoCollectionViewCell class] forCellWithReuseIdentifier:photoCellIndentifier];
}


#pragma mark-
#pragma XHPhotography Helper Method


-(NSMutableArray*)selectPhotos
{
    if(_selectPhotos==nil)
    {
        _selectPhotos=[NSMutableArray array];
    }
    return _selectPhotos;
}


-(XHPhotographyHelper*)photographyHelper{
    if(_photographyHelper==nil){
        _photographyHelper=[[XHPhotographyHelper alloc] init];
    }
    return _photographyHelper;
}


-(void)setUpElements
{

    self.photoCollectionView.layer.cornerRadius = self.descriptionTextView.layer.cornerRadius= self.titleField.layer.cornerRadius      = self.emailTextField.layer.cornerRadius= self.amountTextField.layer.cornerRadius = self.cityTextField.layer.cornerRadius =
    self.contactTextField.layer.cornerRadius =5.0f;
    
    self.photoCollectionView.clipsToBounds  =    self.descriptionTextView.clipsToBounds =
    self.titleField.clipsToBounds           =    self.emailTextField.clipsToBounds=
    self.amountTextField.clipsToBounds      =    self.cityTextField.clipsToBounds =
    self.contactTextField.clipsToBounds     =       YES;
    
    self.descriptionTextView.placeholder= @"Enter Description here...";
    
    self.titleField.attributedPlaceholder= [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Title", @"")                                                                           attributes:@{NSForegroundColorAttributeName: [UIColor darkGrayColor]}];
  
    self.emailTextField.placeholder     =   @"Email";
    self.amountTextField.placeholder    =   @"Amount";
    self.contactTextField.placeholder   =   @"Contact #";
    self.cityTextField.placeholder      =   @"Location";
    
}

#pragma mark-
#pragma mark CollectionView Delegate Methods


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(self.selectPhotos.count==5)
    {
        return self.selectPhotos.count;
    }
    else
    {
        return self.selectPhotos.count+1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    LZAlbumPhotoCollectionViewCell* cell=(LZAlbumPhotoCollectionViewCell*)[collectionView dequeueReusableCellWithReuseIdentifier:photoCellIndentifier forIndexPath:indexPath];
    
    cell.photoImageView.layer.cornerRadius = 5.0f;
    cell.photoImageView.clipsToBounds = YES;
    if(indexPath.row==self.selectPhotos.count)
    {
        
        cell.photoImageView.image=[UIImage imageNamed:@"add_pic@3x"];
        cell.photoImageView.highlightedImage=[UIImage imageNamed:@"add_pic@3x"];
        return cell;
    }else{
        cell.photoImageView.image=self.selectPhotos[indexPath.row];
        cell.photoImageView.highlightedImage=nil;
        return cell;
    }
}


#pragma mark - Delegate
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(kLZAlbumCreateVCPhotoSize, kLZAlbumCreateVCPhotoSize);
}

-(BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    return YES;
}

-(void)addImage:(UIImage*)image{
    [self.selectPhotos addObject:image];
    [self.photoCollectionView reloadData];
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==_selectPhotos.count){
        [self.photographyHelper showOnPickerViewControllerSourceType:UIImagePickerControllerSourceTypePhotoLibrary onViewController:self compled:^(UIImage *image, NSDictionary *editingInfo) {
            if (image) {
                [self addImage:image];
            } else
            {
                if (!editingInfo)
                    return ;
                image=[editingInfo valueForKey:UIImagePickerControllerOriginalImage];
                if(image)
                {
                    [self addImage:image];
                }
            }
        }];
    }
}







- (IBAction)PostAdTouchEvent:(id)sender
{
    
    
}
- (IBAction)Terms:(id)sender
{
    
}
@end
