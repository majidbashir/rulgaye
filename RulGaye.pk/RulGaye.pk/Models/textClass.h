//
//  textClass.h
//  carwash
//
//  Created by Maajid Bashir on 27/09/2014.
//  Copyright (c) 2014 Maajid Bashir. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JJMaterialTextfield.h>


@interface textClass : JJMaterialTextfield


@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@end
