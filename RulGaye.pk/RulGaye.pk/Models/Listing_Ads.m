//
//  Listing_Ads.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 20/01/2016.
//  Copyright © 2016 Cozmuler. All rights reserved.
//

#import "Listing_Ads.h"
#import "Headers.h"


@implementation Listing_Ads


#pragma mark -
#pragma mark Properties Synthesized
@synthesize     item_id         =   _item_id;
@synthesize     title_value     =   _title_value;
@synthesize     desc_value      =   _desc_value;
@synthesize     location        =   _location;
@synthesize     amount          =   _amount ;
@synthesize     date            =   _date;
@synthesize     image           =   _image;
@synthesize     kategory        =   _kategory;
@synthesize     imagesAtIndex   =   _imagesAtIndex;

@synthesize     reviews         =   _reviews;


#pragma mark -
#pragma mark Data_Allocation
- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
/*
     {
     amount = 10000;
     "browser_agent" = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36";
     category = Automobiles;
     city = 116;
     "city_name" = "New Delhi";
     "company_id" = 12;
     date = "2016-01-17 22:57:00";
     description = "I have 8 seater van for sale ";
     fullname = "Rubab Tahir";
     host = "rulgaye.in";
     id = 9396;
     "ip_address" = "115.186.180.146";
     lat = "28.6139391";
     location = "";
     long = "77.2090212";
     "my_images" = "8-seater22.jpg";
     "page_views" = 3;
     rating = "<null>";
     reviews =             (
     );
     state = 10;
     "state_name" = Delhi;
     status = 0;
     "sub_category" = Cars;
     title = "Huawei honor 3c lite";
     "user_id" = 101;
     }
*/
    
    
    
    NSMutableArray * tempArray = [NSMutableArray new];

    if (self)
    {
        self.title_value        =   [dictionary  valueForKey:@"title"];
        self.desc_value         =   [dictionary  valueForKey:@"description"];
        self.location           =   [dictionary  valueForKey:@"city_name"];
        self.amount             =   [dictionary  valueForKey:@"amount"];
//
        if ([self.email isKindOfClass:[NSNull class]])
        {
            self.email          =   @"";
        }
        else
        {
            self.email              =   [dictionary valueForKey:@"email"];
        }
        
        if ([self.phoneNumber isKindOfClass:[NSNull class]] ) {
            self.phoneNumber= @"";
        }
        
        else
        {
            self.phoneNumber        =   [dictionary valueForKey:@"mobile"];
        }
        
        if ([[dictionary valueForKey:@"my_images"] isKindOfClass:[NSNull class]])
        {
            self.image          =  @"";
            self.imagesAtIndex =tempArray;
        }
        else
        {
            NSString * rawImages;
            
            rawImages               =   [dictionary valueForKey:@"my_images"];
            
            self.imagesAtIndex      =   [rawImages componentsSeparatedByString:@","];
            NSString * urlString    =   [NSString stringWithFormat:@"%@",KImageUrls];
            NSArray * tempArr       =   [self prependArrayOfStrings:self.imagesAtIndex prefix:urlString];
            self.imagesAtIndex      =   tempArr;
            
            self.image              =   self.imagesAtIndex[0];
        }
            self.date               =   [dictionary  valueForKey:@"date"];
            self.kategory           =   [dictionary  valueForKey:@"category"];
        }
    
        if ([[dictionary valueForKey:@"reviews"] isKindOfClass:[NSNull class]])
        {
            self.reviews        =    tempArray;
        }
        else
        {
            self.reviews            =   [dictionary valueForKey:@"reviews"];
        }

    return self;
}



- (NSArray *) prependArrayOfStrings:(NSArray*)originalArray prefix:(NSString*)prefix
{
    NSMutableArray *newArray = [[NSMutableArray alloc] init] ;
    for( NSString *currString in originalArray )
    {
        NSString * newString = [NSString stringWithFormat:@"%@%@", prefix, currString];
        [newArray addObject:newString];
    }
    
    return newArray;
}




#pragma mark-
#pragma mark Data_Sending
#pragma mark-

-(NSDictionary*)details_Listing
{
    NSDictionary * detailsDictionary= [NSDictionary dictionaryWithObjects:
  @[
            self.imagesAtIndex,
            self.title_value,
            self.amount,
            self.desc_value,
//            self.email,
            self.phoneNumber,
            self.reviews,
            self.location] forKeys:
  @[
                @"images",
                @"title",
                @"amount",
                @"description",
//                @"email",
                @"phone",
                @"reviews",
                @"location"
                ]];
   return detailsDictionary;
}


@end
