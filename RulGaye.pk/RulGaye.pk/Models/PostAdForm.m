//
//  PostAdForm.m
//
//  Created by Nick Lockwood on 04/03/2014.
//  Copyright (c) 2014 Charcoal Design. All rights reserved.
//

#import "PostAdForm.h"


@implementation PostAdForm 


- (NSArray *)fields
{
    return @[
             //we want to add a group header for the field set of fields
             //we do that by adding the header key to the first field in the group
             
             @{FXFormFieldKey: @"title", FXFormFieldHeader: @"POST AD"},
             
             //we don't need to modify these fields at all, so we'll
             //just refer to them by name to use the default settings
             
             
             //we want to add another group header here, and modify the auto-capitalization
             
             @{FXFormFieldKey: @"name", FXFormFieldHeader: @"Details",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords)},
             
             @"profilePhoto",
             @"phone",
             
             
             
             @{FXFormFieldKey: @"about", FXFormFieldType: FXFormFieldTypeLongText},
             

             
             @{FXFormFieldHeader: @"Legal",
               FXFormFieldTitle: @"Terms And Conditions",
               FXFormFieldSegue: @"TermsSegue"},
             
             @{FXFormFieldTitle: @"Privacy Policy",
               FXFormFieldSegue: @"PrivacyPolicySegue"},
             
             //the automatically generated title (Agreed To Terms) and cell (FXFormSwitchCell)
             //don't really work for this field, so we'll override them both (a type of
             //FXFormFieldTypeOption will use an checkmark instead of a switch by default)
             
             @{FXFormFieldKey: @"agreedToTerms", FXFormFieldTitle: @"I Agree To These Terms", FXFormFieldType: FXFormFieldTypeOption},
           
             
             @{FXFormFieldTitle: @"Submit", FXFormFieldHeader: @"", FXFormFieldAction: @"submitRegistrationForm:"},
             ];
}


@end
