//
//  NetworkClass.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 26/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <JTProgressHUD.h>
#import "Headers.h"



@protocol refreshContent <NSObject>

@required

-(void) refreshContent:(NSDictionary *) JSON;
@end


@interface NetworkClass : NSObject < NSURLConnectionDelegate, NSURLConnectionDelegate>

@property(readwrite,assign) id<refreshContent>delegate;

@property (nonatomic,strong) NSMutableData * responseData;



-(void) requestContent:(NSString *)urlStr view:(UIViewController *)viewCont  page:(NSInteger) pageNumber;
-(void) postRequestContent:(NSString *)urlStr param:(NSString *)postData view:(UIViewController *)viewCont;

@end


