//
//  Headers.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 28/09/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#ifndef Headers_h
#define Headers_h

#define kBaseUrls @"https://www.rulgaye.in/mbteslo/"

#define kNetworkError @"kNetworkError"

#define KImageUrls @"https://www.rulgaye.in/img/"

#define kLoadingCellTag ((int) 12345)

#define kNoDataCellTag ((int) 11111)

#define KUploadImageUrl @""

#define iphone_round    33.0f
#define ipad_round      52.0f

#define navBarColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"navBarColor"]];

#endif 


            /* Headers_h */
