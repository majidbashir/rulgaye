//
//  CategoryManager.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 08/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryManager : NSObject

@property (nonatomic,retain) NSMutableDictionary * cat_dict;

+(id)sharedManager;

@end
