//
//  PostAdForm.h
//  BasicExample
//
//  Created by Nick Lockwood on 04/03/2014.
//  Copyright (c) 2014 Charcoal Design. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FXForms.h"


@interface PostAdForm : NSObject <FXForm>

@property (nonatomic, copy) NSString    *   title;
@property (nonatomic, copy) NSString    *   discription;
@property (nonatomic, strong) UIImage   *   profilePhoto;
@property (nonatomic, copy) NSString    *   phone;
@property (nonatomic, copy) NSString    *   country;
@property (nonatomic, copy) NSString    *   about;
@property (nonatomic, copy) NSString    *   notifications;

@property (nonatomic, assign) BOOL agreedToTerms;

@end
