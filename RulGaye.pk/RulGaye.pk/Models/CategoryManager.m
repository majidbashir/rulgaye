//
//  CategoryManager.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 08/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "CategoryManager.h"

static CategoryManager * sharedManager = nil;

@implementation CategoryManager

@synthesize cat_dict ;

+ (id)sharedManager {
    @synchronized(self)
    {
        
        if(sharedManager == nil)
            sharedManager = [[super allocWithZone:NULL] init];
    }
    return sharedManager;
}

- (id)init
{
    if (self = [super init])
    {
        cat_dict = [[NSMutableDictionary alloc] init];
    }
    return self;
}


@end
