//
//  Listing_Ads.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 20/01/2016.
//  Copyright © 2016 Cozmuler. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Listing_Ads : NSObject


#pragma mark-
#pragma mark property Declarations
@property (nonatomic,copy) NSString * item_id;
@property (nonatomic,copy) NSString * title_value;
@property (nonatomic,copy) NSString * desc_value;
@property (nonatomic,copy) NSString * amount;
@property (nonatomic,copy) NSString * location;
@property (nonatomic,copy) NSString * date;
@property (nonatomic,copy) NSString * image;
@property (nonatomic,copy) NSString * kategory;


@property (nonatomic,copy) NSArray  * imagesAtIndex ,* reviews;


@property (nonatomic,copy) NSString * phoneNumber,* email;



#pragma mark-
#pragma mark allocating values
- (id)initWithDictionary:(NSDictionary *)dictionary;


#pragma mark-
#pragma mark fetching values
-(NSDictionary*)details_Listing;

@end
