//
//  NetworkClass.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 26/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "NetworkClass.h"

@implementation NetworkClass


UIViewController * currentCont;


-(void)requestContent:(NSString *)urlStr view:(UIViewController *)viewCont page:(NSInteger)pageNumber
{
    currentCont = viewCont;
    
    pageNumber = 0;
    
    [JTProgressHUD show];
    
    NSURL * apiURL = [NSURL URLWithString:urlStr];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:apiURL];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               if(data.length)
                               {
                                   NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   
                                   if(responseString && responseString.length)
                                   {
                                       NSDictionary * JSON = [[NSDictionary alloc]init];
                                       JSON = [NSJSONSerialization JSONObjectWithData: [responseString dataUsingEncoding:NSUTF8StringEncoding]
                                                                              options: NSJSONReadingMutableContainers
                                                                                error: nil];
                                       if( self.delegate && [[self delegate] respondsToSelector:@selector(refreshContent:)])
                                           [[self delegate]refreshContent:JSON];
                                   }
                               }
                               else if(connectionError)
                               {
                                   NSLog(@"NETWORK error: %@",connectionError.debugDescription);
                               }
                              
                               [JTProgressHUD hide];
                               
                           }];
    
}


-(void) postRequestContent:(NSString *)urlStr param:(NSString *)postData view:(UIViewController *)viewCont
{
    currentCont = viewCont;
    
    [JTProgressHUD show];
    
    // Create the request.
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    
    //set http method
    [request setHTTPMethod:@"POST"];

    //initialize a post data
    [request setValue:@"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    
    //set post data of request
    [request setHTTPBody:[postData dataUsingEncoding:NSUTF8StringEncoding]];
    
    // Create url connection and fire request
    NSURLConnection *conn = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    [conn start];
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    self.responseData = [[ NSMutableData alloc] init];
}


- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
  
    // Append the new data to the instance variable you declared
    [self.responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
  
    [JTProgressHUD hide];
    
    NSString *JSONString = [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding];
    NSDictionary *JSON = [[NSDictionary alloc]init];
    
    JSON = [NSJSONSerialization JSONObjectWithData: [JSONString dataUsingEncoding:NSUTF8StringEncoding]
                                           options: NSJSONReadingMutableContainers
                                             error: nil];
    
    //below only calling the method but it is impelmented in AwindowController class
   
    if([[self delegate]respondsToSelector:@selector(refreshContent:)])
        [[self delegate]refreshContent:JSON];
    
}


- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [JTProgressHUD hide];
    NSString *errorStr = @"Something goes wrong please check your network.";
    NSLog(@"network error: %@",errorStr);
    [[NSNotificationCenter defaultCenter] postNotificationName:kNetworkError object:errorStr];
    
}


@end

