//
//  NavigationHandler.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 28/09/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "NavigationHandler.h"
#import "CategoryVC.h"



@implementation NavigationHandler

@synthesize noNetwork;


-(void)viewDidLoad
{
    [super viewDidLoad];
    
//    [self hello];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(alertStatus:) name:@"ShowAlert" object:nil];

    noNetwork.frame= CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width);
    noNetwork.backgroundColor= [UIColor grayColor];
    UILabel * networkLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.width)];
    
    [noNetwork addSubview:networkLabel];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        UIImageView * titleLogo =[[UIImageView alloc]initWithFrame:CGRectMake(85, -5, 50, 50)];
        [titleLogo setImage: [UIImage imageNamed:@"app_icon_top"]];
        [self.navigationController.navigationBar addSubview:titleLogo];
    }
    
    else
    {
        UIImageView * titleLogo =[[UIImageView alloc]initWithFrame:CGRectMake(80, 5, 35, 35)];
        [titleLogo setImage: [UIImage imageNamed:@"app_icon_top"]];
        [self.navigationController.navigationBar addSubview:titleLogo];
    }

    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:255/255.0f green:135/255.0f blue:0/255.0f alpha:0.25f]];
  
    
   
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:225/255.0f green:136/255.0f blue:0/255.0f alpha:1];

    
}





-(void)alertStatus :(NSNotification*)sender
{
    if ([UIAlertController class])
    {
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:
                                              [sender.userInfo objectForKey:@"title"]
                                                message:[sender.userInfo objectForKey:@"message"]preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        [alertController addAction:ok];
        
        [self presentViewController:alertController animated:YES completion:nil];
        
    }
   
    else
    
    {
        UIAlertView * alert = [[UIAlertView alloc]initWithTitle:@"Alert title" message:@"Alert message" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}


- (void) hello
{
//    Reachability * reachability;
//    NetworkStatus netStatus = [reachability currentReachabilityStatus];
//    BOOL connectionRequired = [reachability connectionRequired];
//    NSString* statusString = @"";
//    
//    
//    switch (netStatus)
//    {
//        case NotReachable:        {
//            statusString = NSLocalizedString(@"Access Not Available", @"Text field text for access is not available");
//            
//            connectionRequired = NO;
//            break;
//        }
//            
//        case ReachableViaWWAN:        {
//            statusString = NSLocalizedString(@"Reachable WWAN", @"");
//            break;
//        }
//        case ReachableViaWiFi:        {
//            statusString= NSLocalizedString(@"Reachable WiFi", @"");
//            break;
//        }
//    }
    
}



@end
