//
//  NavigationHandler.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 28/09/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Headers.h"
#import <AFNetworking/AFNetworking.h>
#import "Reachability.h"




@interface NavigationHandler : UIViewController

@property(nonatomic,strong) UIView * noNetwork;

@end
