//
//  main.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 03/09/2015.
//  Copyright (c) 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool
    {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
