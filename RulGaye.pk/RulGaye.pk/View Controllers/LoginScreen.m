//
//  LoginScreen.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 16/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "LoginScreen.h"
#import <JTProgressHUD.h>
#import "RegisterVC.h"
#import "ForgetPasswordVC.h"


@implementation LoginScreen


-(void)viewDidLoad
{
    [super viewDidLoad];

}
- (IBAction)signInTouchUpInside
{
    [self makeRequest];
}

- (IBAction)forgetPassTouchUpInside
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
    ForgetPasswordVC *add =[storyboard instantiateViewControllerWithIdentifier:@"ForgetPasswordVC"];
    
    [self presentViewController:add animated:YES completion:nil];
}
 - (IBAction)registerButtonTouchUpInside
{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    RegisterVC *add = [storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];

    [self presentViewController:add animated:YES completion:nil];
}

-(void)makeRequest
{
    if ([self.emailField.text isEqualToString:@""] && [self.passwordField.text isEqualToString:@""])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":@"All Fields required"}];
    }
    else
        {
        
            @try
                {
                    [JTProgressHUD show];
                    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                    manager.responseSerializer.acceptableContentTypes=[manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
                

                    NSDictionary * param = [NSDictionary dictionaryWithObjectsAndKeys:self.emailField.text,@"email",self.passwordField.text,@"password", nil];
                    [manager POST:(kBaseUrls @"signIn") parameters:param progress:nil success:^(NSURLSessionTask *task,id responseObject)
                {
                        NSLog(@"Response from server: %@",responseObject);
                    
                 
                 if ([[responseObject valueForKey:@"status"] isEqualToString:@"OK"])
                     
                    {
                        NSString   * user      = [responseObject valueForKey:@"id"];
                        NSUserDefaults * defaults  = [NSUserDefaults standardUserDefaults];
                        [defaults setObject:user forKey:@"user_id"];
                        [defaults synchronize];
                   
                        [JTProgressHUD hide];
                        [self dismissViewControllerAnimated:YES completion:nil];

                    }
                 else if( [[responseObject valueForKey:@"status"] isEqualToString:@"INVALID"])
                 {
                    
                     [JTProgressHUD hide];

                     [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":@"Email or Password is Wrong.."}];
 
                 }

                    
                }
         
            failure:^(NSURLSessionTask *operation, NSError *error)
             {
                    [JTProgressHUD hide];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":[[error userInfo]valueForKey:@"NSLocalizedDescription"]}];
                    localError= nil;
            }];
        }
                    @catch (NSException * exception)
        {
                [JTProgressHUD hide];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":exception}];
        }
    }

    }

@end
