//
//  browseCell.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 07/10/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface browseCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView    *       image_placeholder;
@property (weak, nonatomic) IBOutlet UILabel        *       titleLabel;
@property (weak, nonatomic) IBOutlet UILabel        *       locationLabel;
@property (weak, nonatomic) IBOutlet UILabel        *       descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel        *       dateLabel;
@property (weak, nonatomic) IBOutlet UILabel        *       priceLabel;
@property (strong, nonatomic) IBOutlet UIButton     *       favouriteButton;

@property (nonatomic , readwrite) BOOL isChecked;

@end
