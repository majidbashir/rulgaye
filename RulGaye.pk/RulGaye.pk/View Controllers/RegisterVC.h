//
//  RegisterVC.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 17/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationHandler.h"
#import <JTProgressHUD.h>
#import <AFNetworking.h>


@interface RegisterVC : NavigationHandler
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *lastNameField;
@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *repeatPasswordField;
- (IBAction)RegisterTouchUpInside;
- (IBAction)canceTouchUpInside;

@end
