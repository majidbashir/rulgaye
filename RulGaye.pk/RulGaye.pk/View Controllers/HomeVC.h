//
//  HomeVC.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 22/09/2015.
//  Copyright (c) 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationHandler.h"
#import "CategoryManager.h"
#import "CategoryVC.h"




@interface HomeVC :NavigationHandler

{
    NSMutableDictionary * allCategories;
    NSMutableArray * categoryArray;
}
- (IBAction)buttonTouchUpInside:(id)sender;
- (IBAction)browseTouchUpInside:(id)sender;


@end
