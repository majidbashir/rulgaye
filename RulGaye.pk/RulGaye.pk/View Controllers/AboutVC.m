//
//  ViewController.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 03/09/2015.
//  Copyright (c) 2015 Cozmuler. All rights reserved.
//

#import "AboutVC.h"

@interface AboutVC ()

@end

@implementation AboutVC
@synthesize AboutTV;
- (void)viewDidLoad
{
    [super viewDidLoad];

}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{

    UIView * bgView;

    
    if (section==0)
    {
        bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width-15, 270)];
        
        UIImageView * bgImageView= [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 250)];
      
        
        
        
        bgImageView.image= [UIImage imageNamed:@"ali.jpg"];
        [bgView addSubview:bgImageView];
    }
    
    
//    else if (section==1)
//    {
//        
//        bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 20)];
//    
//
//    }
//
//    else if (section==2)
//    {
//        bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width-15, 20)];
//    }
//    
//    else if (section==3)
//    {
//        bgView =[[UIView alloc]initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width-15, 20)];
//    }
//    
    return bgView;
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    aboutCell * cell= [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.userStateSwitch.hidden= YES;
    
    cell.layer.borderColor=[[UIColor colorWithRed:214/255.0f green:116/255.0f blue:2/255.0f alpha:1]CGColor];
    cell.layer.borderWidth= 2.0f;
    
    
    
    if (indexPath.section==0 && indexPath.row==0)
    {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        
        NSString * currentUser_ID = [defaults objectForKey:@"user_id"];
        
        NSLog(@"Current User ID is:%@",currentUser_ID);
        
        
        if (currentUser_ID)
        {
            cell.userStateSwitch.hidden= NO;
            cell.userStateSwitch.on= YES;
            [cell.userStateSwitch addTarget:self action:@selector(setState:) forControlEvents:UIControlEventValueChanged];

            if (cell.userStateSwitch.on)
            {
                cell.ItemLabel.text =@"Log Out";
            }
            else
            {
                cell.ItemLabel.text=@"Login";
            }
            
        }

    }
    
    
   else if (indexPath.row==0 && indexPath.section ==1)
    {
     cell.ItemLabel.text =@"About Rulgaye";
        
    }
    
   else if (indexPath.row==0 && indexPath.section ==2)
   {
       cell.ItemLabel.text =@"About Rulgaye";
       
   }
   else if (indexPath.row==0 && indexPath.section ==3)
   {
       cell.ItemLabel.text =@"terms & Conditions";
       
   }
    
    return cell;
}


-(void)setState:(id)sender
{
    BOOL state = [sender isOn];
    
    NSString *rez = state == YES ? @"YES" : @"NO";
    NSLog(@"%@",rez);

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section ==0)
    {
        return 270;
    }

    return 20;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{

    return 1;
}


@end
