//
//  subCatVC.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 20/10/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "CategoryVC.h"




@implementation CategoryVC




@synthesize categoryArray;
@synthesize itemid;
@synthesize subCategoryTableView;
@synthesize allCategories;



-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    NSLog(@"item ID is: %ld",(long)itemid);
    [super viewWillAppear:YES];
    
    CategoryManager * item = [CategoryManager sharedManager];
    allCategories = [item.cat_dict valueForKey:@"sub"];
    categoryArray =  [[[item.cat_dict valueForKey:@"sub"]objectAtIndex:itemid-1]valueForKey:@"name"];
}

-(void)viewDidLoad
{
    [super viewDidLoad];

}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    categoryListingcell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.listingLabel.text = [categoryArray objectAtIndex:indexPath.row];
   
    cell.listingLabel.textColor=[UIColor grayColor];
    cell.listingLabel.font= [UIFont fontWithName:@"Avenir-Heavy" size:15];
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  40;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [categoryArray count];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CategoryManager * item = [CategoryManager sharedManager];
   
    NSInteger item_id= [[[[[item.cat_dict valueForKey:@"sub"]objectAtIndex:itemid-1]objectAtIndex:indexPath.row]valueForKey:@"id"]integerValue];
    
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    BrowseVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"BrowseVC"];
    
    
    vc.search_item_id = item_id;
    [self.navigationController pushViewController:vc animated:YES];

}

@end
