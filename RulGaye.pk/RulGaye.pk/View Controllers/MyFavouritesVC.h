//
//  MyFavouritesVC.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 13/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationHandler.h"


@interface MyFavouritesVC :  NavigationHandler <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate>

{
    BOOL isChecked;
}

@property (weak, nonatomic) IBOutlet UIView *NotificationView;

@property(nonatomic, assign) NSInteger category_ID;

@property (weak, nonatomic) IBOutlet UITableView *listingTableView;
@property IBOutlet UISearchBar * searchBar;


@end
