//
//  MyFavouritesVC.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 13/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "MyFavouritesVC.h"
#import "browseCell.h"
#import "CategoryManager.h"
#import <JTProgressHUD.h>




@implementation MyFavouritesVC

{
    
    NSMutableDictionary * data;
    NSMutableArray * favourites;
    NSString * localError;
    
    
}

@synthesize listingTableView;

@synthesize searchBar;
@synthesize NotificationView;


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    [searchBar setShowsScopeBar:NO];
    [searchBar sizeToFit];
    
    CategoryManager * item = [CategoryManager sharedManager];
    NSLog( @"categories include %@",item.cat_dict);
    
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];

    if ([AFNetworkReachabilityManager sharedManager].isReachable)
    {
        [NotificationView setHidden:YES];
        [self makeRequest];
    }
    
    else
    {
        [NotificationView setHidden:false];
    }
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    browseCell * cell = [tableView dequeueReusableCellWithIdentifier:@"browse_cell" forIndexPath:indexPath];
    cell.selectionStyle= UITableViewCellSelectionStyleNone;
    
    
    cell.favouriteButton.tag=indexPath.row;
    
    cell.titleLabel.text = [[[data valueForKey:@"items"]valueForKey:@"title"] objectAtIndex:indexPath.row];
    
    cell.priceLabel.text = [NSString stringWithFormat:@"₹ %@",[[[data valueForKey:@"items"]valueForKey:@"amount"] objectAtIndex:indexPath.row]];
    
    cell.dateLabel.text = [[[data valueForKey:@"items"]valueForKey:@"date"] objectAtIndex:indexPath.row];
    
    cell.descriptionLabel.text = [[[data valueForKey:@"items"]valueForKey:@"description"] objectAtIndex:indexPath.row];
    
    cell.locationLabel.text =[[[data valueForKey:@"items"]valueForKey:@"location"] objectAtIndex:indexPath.row];
    
    cell.image_placeholder.image= [UIImage imageNamed:@"Classified_avatar.png"];
    
    NSLog(@"Avatars to be shown: %@",[[[data valueForKey:@"items"]valueForKey:@"category"]objectAtIndex:indexPath.row]);
    
    
//    cell.image_placeholder.image= [NSString stringWithFormat:@"%@.png"[[[[data valueForKey:@"items"]valueForKey:@"category"]objectAtIndex:indexPath.row] isEqualToString:@""]];
    
    if (![favourites containsObject:[NSString stringWithFormat:@"%ld", (long)indexPath]])
        
    {
        [favourites addObject:[NSString stringWithFormat:@"%ld", (long)indexPath]];
    }
    
    else
        
    {
        
        [favourites removeObject:[NSString stringWithFormat:@"%ld", (long)indexPath]];
    }
    
    return cell;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return [[[data valueForKey:@"items"]valueForKey:@"title"] count];
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [JTProgressHUD hide];
}






-(void)makeRequest
{
    [JTProgressHUD show];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
    [manager GET: (kBaseUrls @"getUserAds/10") parameters:nil progress:nil success:^
     (NSURLSessionTask * operation, id responseObject)
     {
         data = responseObject;
         NSLog(@"Response from SErver:%@",data);
         
         [JTProgressHUD hide];
         
         [listingTableView reloadData];
         
     }
        failure:^(NSURLSessionTask * operation, NSError *error)
     {
         localError = [[error userInfo]valueForKey:@"NSLocalizedDescription"];
         if (localError)
         {
             [JTProgressHUD hide];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"Hey...",@"message":localError}];
         }
     }
     ];
    
}


@end

