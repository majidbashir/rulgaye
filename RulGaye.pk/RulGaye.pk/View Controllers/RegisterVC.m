//
//  RegisterVC.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 17/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "RegisterVC.h"

@implementation RegisterVC

- (IBAction)RegisterTouchUpInside
{
    
    [self makeRequest];
}


- (IBAction)ForgetPasswordTouchUpInside
{
}

- (IBAction)canceTouchUpInside
{
[self dismissViewControllerAnimated:YES
                         completion:nil];
}


-(void)makeRequest
{
    {
        if ([self.emailField.text isEqualToString:@""] && [self.firstNameField.text isEqualToString:@""])
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":@"All Fields required"}];
        }
        else
        {
            @try
            {
                
                [JTProgressHUD show];
                
                
                NSDictionary * param = [NSDictionary dictionaryWithObjectsAndKeys:
                                        self.emailField.text,@"email",
                                        self.firstNameField.text,@"fname",
                                        self.lastNameField.text,@"lname",
                                        self.repeatPasswordField.text,@"password",
                                        nil];

                
                
                AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
                
                manager.responseSerializer.acceptableContentTypes=[manager.responseSerializer.
                                                                   acceptableContentTypes setByAddingObject:@"text/html"];
                
                [manager POST:(kBaseUrls @"signUp") parameters:param progress : nil  success:^(NSURLSessionTask *task, id responseObject)
                 {
                     
                     NSLog(@"%@",[responseObject valueForKey:@"status"]);
                     
                     [JTProgressHUD hide];
                 }
                 
                      failure:^(NSURLSessionTask *operation, NSError *error)
                 {
                     [JTProgressHUD hide];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":[[error userInfo]valueForKey:@"NSLocalizedDescription"]}];
//                     localError= nil;
                 }];
            }
            
            @catch (NSException * exception)
            
            {
                [JTProgressHUD hide];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":exception}];
            }
        }
        
    }

}



@end
