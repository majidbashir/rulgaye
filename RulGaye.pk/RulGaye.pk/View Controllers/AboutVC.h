//
//  ViewController.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 03/09/2015.
//  Copyright (c) 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "aboutCell.h"

@interface AboutVC : UIViewController<UITableViewDelegate,UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *AboutTV;

@end

