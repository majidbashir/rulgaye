//
//  ForgetPasswordVC.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 17/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NavigationHandler.h"
#import <AFNetworking.h>


@interface ForgetPasswordVC : NavigationHandler
@property (weak, nonatomic) IBOutlet UITextField *resetEmailField;
- (IBAction)resetPassTouchUpInside;
- (IBAction)cancelButtonTouchUpInside;

@end
