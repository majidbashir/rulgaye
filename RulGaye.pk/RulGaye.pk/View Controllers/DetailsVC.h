//
//  DetailsVC.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 08/01/2016.
//  Copyright © 2016 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KIImagePager.h>
#import <TGFoursquareLocationDetail.h>
#import "NavigationHandler.h"
#import "Listing_Ads.h"
#import "DynamicTableViewCell.h"


@class Listing_Ads;


@interface DetailsVC : NavigationHandler<UITableViewDataSource,UITableViewDelegate, KIImagePagerDelegate, KIImagePagerDataSource>
@property (weak, nonatomic) IBOutlet UITableView * DetailPageTableView;

@property (nonatomic, strong) TGFoursquareLocationDetail * locationDetail;
@property (weak, nonatomic) IBOutlet KIImagePager *headerView;

@property(strong,nonatomic) Listing_Ads * detailsListing;

@end
