//
//  LoginScreen.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 16/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>
#import "NavigationHandler.h"

@interface LoginScreen : NavigationHandler
{
    NSString * localError;
}

@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
- (IBAction)signInTouchUpInside;
- (IBAction)forgetPassTouchUpInside;
- (IBAction)registerButtonTouchUpInside;


@end
