//
//  CreateScheduleVC.m
//  Smart Sales
//
//  Created by Maajid Bashir on 20/03/2015.
//  Copyright (c) 2015 Smart Intellect. All rights reserved.
//

#import "CreateScheduleVC.h"

@interface CreateScheduleVC () <UITableViewDataSource,UITableViewDelegate>
{
    NSArray * placeholders;
}
@end

@implementation CreateScheduleVC

- (void)viewDidLoad

{
    
    
    placeholders= [NSArray arrayWithObjects:
                   @"Takeaway Name",
                   @"Owner",
                   @"Phone #",
                   @"Address",
                   @"Meeting Date",
                   @"Meeting Time",
                   @"Notes",
                   nil];
    
    
    
    [super viewDidLoad];
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CreateScheduleCell* cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.user_text_field.placeholder= [placeholders objectAtIndex:indexPath.row];
    
    return cell;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [placeholders count];
    
}



@end
