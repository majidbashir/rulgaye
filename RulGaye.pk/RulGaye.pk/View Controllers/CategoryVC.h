//
//  subCatVC.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 20/10/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "CategoryManager.h"
#import "BrowseVC.h"
#import "categoryListingcell.h"
#import "NavigationHandler.h"






@interface CategoryVC : NavigationHandler<UITableViewDataSource, UITableViewDelegate>



@property (nonatomic,strong) NSMutableArray * categoryArray;

@property(weak,nonatomic)NSMutableDictionary * allCategories;

@property (weak, nonatomic) IBOutlet UITableView *subCategoryTableView;
@property(nonatomic,assign) NSInteger itemid;

@end
