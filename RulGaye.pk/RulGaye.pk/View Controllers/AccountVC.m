//
//  AccountVC.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 20/10/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "AccountVC.h"
#import <AFNetworking/AFNetworking.h>


@implementation AccountVC

{
    NSArray * loginPlaceHolders,* registerPlaceholders, * forgetPlaceholders ;
    
}

-(void)viewDidLoad

{
    [super viewDidLoad];
    

    
    registerPlaceholders = [NSArray arrayWithObjects:
                   @"Email address..",
                   @"Full Name",
                   @"Password",
                   @"Confirm Password",
                   nil];

    loginPlaceHolders = [NSArray arrayWithObjects:
                         @"Email address",
                         @"Password"
                         ,nil];
    
    forgetPlaceholders = [NSArray arrayWithObjects:
                          @"Email Address",
                          nil];
}

/*
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AccountCell * cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    cell.textField.tintColor = [UIColor orangeColor];
    cell.textField.placeholder= [registerPlaceholders objectAtIndex:indexPath.row];
    
    if (indexPath.row == 2)
    {
        cell.textField.secureTextEntry= YES;

    }
    
    // Design
    cell.textField.layer.borderColor=[[UIColor colorWithRed:214/255.0f green:116/255.0f blue:2/255.0f alpha:1]CGColor];
    cell.textField.layer.borderWidth= 2.0f;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
*/

// Number of Fields

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    return  4;
}

// Height for header

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 150;
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    // Create a view with background color..
    
    UIView *sectionHeaderView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, tableView.bounds.size.width,100)];
    sectionHeaderView.backgroundColor = [UIColor orangeColor];
    sectionHeaderView.tintColor= [UIColor orangeColor];
    
    
    
    // Image Created before the title label in center of view
    UIImageView * imageview = [[UIImageView alloc] initWithFrame:
                            CGRectMake(0 , 0, tableView.bounds.size.width, 105)];
    [imageview setImage:[UIImage imageNamed:@"form_top_img@3x.png"]];
    imageview.autoresizingMask = UIViewAutoresizingFlexibleWidth;
   
    
   
    // Label Created ater the title image in center of view
    
    
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.opaque = NO;
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.highlightedTextColor = [UIColor whiteColor];
    headerLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Bold" size:22];
    headerLabel.frame = CGRectMake(0, 113, tableView.bounds.size.width, 30);
    headerLabel.textAlignment= NSTextAlignmentCenter;
    headerLabel.text = @"SIGN UP";
    
    headerLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth ;

    [sectionHeaderView addSubview:headerLabel];
    [sectionHeaderView addSubview: imageview];
   
    return sectionHeaderView;
}


-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView * myFooterView;
    
    
    myFooterView = [[UIView alloc] initWithFrame: CGRectMake(0, 0, tableView.bounds.size.width,100)];
    
    UIButton * registerButton= [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-25, 10, 50, 25)];
    registerButton.backgroundColor= [UIColor greenColor];
    [myFooterView addSubview:registerButton];
    
    return myFooterView;
}


@end
