
//  HomeVC.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 22/09/2015.
//  Copyright (c) 2015 Cozmuler. All rights reserved.
//

#import "HomeVC.h"
#import "Headers.h"



@interface HomeVC ()


{
    NSString * localError;

}

@property (weak, nonatomic) IBOutlet UIButton *classified;
@property (weak, nonatomic) IBOutlet UIButton *career;

@property (weak, nonatomic) IBOutlet UIButton *property;
@property (weak, nonatomic) IBOutlet UIButton *vehicles;
@property (weak, nonatomic) IBOutlet UIButton *browseButton;

@property(nonatomic,assign) NSInteger categoryID;

@end

@implementation HomeVC

@synthesize categoryID;


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    if ([[UIDevice currentDevice]userInterfaceIdiom] == UIUserInterfaceIdiomPad)
    {
        self.classified.layer.cornerRadius = self.career.layer.cornerRadius =self.property.layer.cornerRadius= self.vehicles.layer.cornerRadius =  ipad_round;
        self.browseButton.layer.cornerRadius= 15.0f;
        self.browseButton.clipsToBounds = YES;

    }
    else
    {
        self.classified.layer.cornerRadius = self.career.layer.cornerRadius =self.property.layer.cornerRadius= self.vehicles.layer.cornerRadius =  iphone_round;
        self.browseButton.layer.cornerRadius= 5.0f;
        self.browseButton.clipsToBounds = YES;
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.vehicles setTag:1];
    [self.classified setTag:2];
    [self.property setTag:3];
    [self.career setTag:4];
}


- (IBAction)buttonTouchUpInside:(UIButton *)sender
{

    categoryID = [sender tag];
  
    NSLog(@"category ID is : %d",categoryID);
    
    if (categoryID)
    {
        UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        CategoryVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
        vc.itemid = [sender tag];
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)browseTouchUpInside:(id)sender
{
    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    BrowseVC * vc = [storyboard instantiateViewControllerWithIdentifier:@"BrowseVC"];
    
    vc.search_item_id = 1212;
    [self.navigationController pushViewController:vc animated:YES];

}


@end
