//
//  BrowseVC.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 01/10/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "BrowseVC.h"
#import <JTProgressHUD.h>
#import "NetworkClass.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "DetailsVC.h"
#import "Listing_Ads.h"
#import "LoginScreen.h"



@interface BrowseVC ()
{
    NSMutableDictionary *   data;
    NSString            *   localError;
    NSArray             *   items;
    NSArray             *   vcStack;
    NSString            *   currentUser_ID;
    
    
}
@property (nonatomic, retain) NSMutableArray * ads_Array;

@end

@implementation BrowseVC

@synthesize ads_Array= _ads_Array;

@synthesize listingTableView;
@synthesize urlString= _urlString;
@synthesize searchBar;
@synthesize NotificationView;
@synthesize favourites;
@synthesize search_item_id;
@synthesize  added2FavArray = _added2FavArray;

-(void)viewDidLoad
{
    [super viewDidLoad];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    currentUser_ID = [defaults objectForKey:@"user_id"];

    NSLog(@"Current User ID is:%@",currentUser_ID);
    
    vcStack = self.navigationController.viewControllers;

    if ([vcStack count] == 1 && !currentUser_ID)
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
        LoginScreen *add =[storyboard instantiateViewControllerWithIdentifier:@"LoginScreen"];
        [self presentViewController:add animated:YES completion:nil];
    }
    else
    {
        _currentPage =1;
    
        [self makeRequest];
    }
    
    self.ads_Array= [NSMutableArray array];
    _currentPage= 1;
    
    self.added2FavArray =[NSMutableArray array];

}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    currentUser_ID = [defaults objectForKey:@"user_id"];
    
    NSLog(@"Current User ID is:%@",currentUser_ID);
    
    if ([vcStack count] == 1 && !currentUser_ID)
    {
        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main"bundle:nil];
        LoginScreen *add =[storyboard instantiateViewControllerWithIdentifier:@"LoginScreen"];
        [self presentViewController:add animated:YES completion:nil];
    }
    else
    {
        _currentPage =1;
        [self makeRequest];
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row < [_ads_Array count])
    {
       return [self itemsCell:indexPath];
    }
    
    else if ([_ads_Array count]==0)
    
    {
        return [self noDataCell];
    }

    else
    {
        return [self loadingCell];
    }
    

}


#pragma mark -
#pragma mark Custom_Cells

- (UITableViewCell *)noDataCell
{
    UITableViewCell * cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil] ;
    
    cell.frame= CGRectMake(0, 0, self.listingTableView.frame.size.width,self.listingTableView.frame.size.height );
    cell.textLabel.text= @"NO Records Found";
    cell.textLabel.font= [UIFont fontWithName:@"" size:30];
    cell.textLabel.center= self.view.center;
    cell.tag= kNoDataCellTag;
    return cell;
}

- (UITableViewCell *) loadingCell
{
    UITableViewCell * cell = [[UITableViewCell alloc]
                             initWithStyle:UITableViewCellStyleDefault
                             reuseIdentifier:nil] ;
    
    UIActivityIndicatorView * activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    activityIndicator.center = cell.center;
    [cell addSubview:activityIndicator];
    
    [activityIndicator startAnimating];
    
    cell.tag = kLoadingCellTag;
    
    return cell;
}

- (UITableViewCell *)itemsCell:(NSIndexPath *)indexPath
{
   
    static NSString *cellIdentifier = @"browse_cell";
    
    browseCell *cell = [self.listingTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    Listing_Ads * ads = [self.ads_Array objectAtIndex:indexPath.row];
    
    //1
    cell.titleLabel.text = ads.title_value;
    cell.titleLabel.font= [UIFont fontWithName:@"Avenir-Heavy" size:14];

    //2
    cell.descriptionLabel.text = ads.desc_value;
    
    //3
    cell.locationLabel.text= ads.location;
    
    //4
    
    
    cell.image_placeholder.clipsToBounds= YES;
    
    if ([ads.kategory isEqualToString:@"Jobs"])
    {
        [cell.image_placeholder sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",ads.image]] placeholderImage:[UIImage imageNamed:@"jobs_avatar.png"]];
    }
    
    else if ([ads.kategory isEqualToString:@"Automobiles"])
    {
        [cell.image_placeholder sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",ads.image]] placeholderImage:[UIImage imageNamed:@"vehicles_avatar.png"]];
    }

    else if ([ads.kategory isEqualToString:@"Classifieds"])
    {
        [cell.image_placeholder sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",ads.image]] placeholderImage:[UIImage imageNamed:@"Classified_avatar.png"]];
    }

    else if ([ads.kategory isEqualToString:@"Real Estate"])
    {
        [cell.image_placeholder sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",ads.image]] placeholderImage:[UIImage imageNamed:@"property_avatar.png"]];
    }

    //5
    cell.dateLabel.text = ads.date;
    cell.priceLabel.text = [NSString stringWithFormat:@"₹ %@", ads.amount];
    [cell.favouriteButton addTarget:self action:@selector(btnCommentClick:) forControlEvents:UIControlEventTouchUpInside];
    cell.favouriteButton.tag= indexPath.row;

    return cell;
}



#pragma mark -
#pragma mark Append_More_Data

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (cell.tag == kLoadingCellTag)
    {
        _currentPage++;
        [self makeRequest];
    }

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([self.ads_Array count]==0) {
        
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = @"No data is currently available Yet.";
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:20];
        [messageLabel sizeToFit];
        
        self.listingTableView.backgroundView = messageLabel;
        self.listingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

        return 0;
    }
    else
    {
    
    if (_currentPage < _totalPages)
    {
        return self.ads_Array.count+1;
    }
    
        return self.ads_Array.count;
    }

}
-(void)makeRequest
{
    if (_currentPage == 1)
        {
            [JTProgressHUD show];
        }
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        
        manager.responseSerializer.acceptableContentTypes=[manager.responseSerializer.
                                                           acceptableContentTypes setByAddingObject:@"text/html"];
  
        switch ([vcStack count])
        {
            case 1:
            
                self.urlString =[NSString stringWithFormat:@"%@getUserAds/%@/%d",kBaseUrls,currentUser_ID,_currentPage];
                break;
            
            case 2:
                
                self.urlString= [NSString stringWithFormat:@"%@search?search=jobs&page=%d", kBaseUrls,_currentPage];
                break;
                
            case 3:
                self.urlString= [NSString stringWithFormat:@"%@getItems/%ld/%d", kBaseUrls, (long)search_item_id,_currentPage];
                break;
                
            default:
                break;
        }
        
        NSLog(@"URL HERE : %@",self.urlString);
    
        
        [manager GET:self.urlString parameters:nil progress:nil success:^(NSURLSessionTask * task, id responseObject)
         {
             
             NSLog(@"Response for #%d request is: %@",_currentPage,responseObject);
             
             _totalPages = [[responseObject objectForKey:@"total_pages"]integerValue];
            
             for (int i = 0;i<[[responseObject valueForKey:@"items"]count];i++)

             {
                 Listing_Ads * ads = [[Listing_Ads alloc] initWithDictionary:[responseObject objectForKey:@"items"][i]];
                 if (![self.ads_Array containsObject:ads])
                 {
                     [self.ads_Array addObject:ads];
                 }
             }
             [JTProgressHUD hide];
             [listingTableView reloadData];
        
         }
             failure:^(NSURLSessionTask * operation, NSError *error)
         {
             [JTProgressHUD hide];
             [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..",@"message":@"Something went wrong.\n Please check your Internet.. Connection"}];
         }];
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"showDetail"])
    {
        NSIndexPath *indexPath = [self.listingTableView indexPathForSelectedRow];
        Listing_Ads * selectedItem = self.ads_Array[indexPath.row];
        [[segue destinationViewController] setDetailsListing:selectedItem];
    }
}


-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [JTProgressHUD hide];
}

@end
