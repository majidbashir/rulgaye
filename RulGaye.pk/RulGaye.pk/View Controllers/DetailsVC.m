//
//  ViewController.m
//  TGFoursquareLocationDetail-Demo
//
//  Created by Thibault Guégan on 15/12/2013.
//  Copyright (c) 2013 Thibault Guégan. All rights reserved.
//

#import "DetailsVC.h"
#import "DetailValueCell.h"



//static NSInteger const kImagePagerMinHeight = 200.0f;

@interface DetailsVC ()




@end


static NSString* const CellIdentifier = @"DynamicTableViewCell";


@implementation DetailsVC

@synthesize headerView;
@synthesize detailsListing;





- (void)setUpCell:(DynamicTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    cell.titleLabel.textColor=[UIColor grayColor];
    cell.titleLabel.font= [UIFont fontWithName:@"Avenir-Heavy" size:15];
    cell.description_Label.numberOfLines = 0;
    
    
    
    if (indexPath.row==0)
    {
        cell.titleLabel.text= @"Title";
        cell.description_Label.text=[detailsListing.details_Listing valueForKey:@"title"];
    }
    
    else if (indexPath.row==1)
    {
        cell.titleLabel.text= @"Description";
        cell.description_Label.text=[detailsListing.details_Listing valueForKey:@"description"];
    }
    
    
    else if (indexPath.row==2)
    {
        cell.titleLabel.text= @"Amount";
        cell.description_Label.text=
        [NSString stringWithFormat:@"₹ %@", [detailsListing.details_Listing valueForKey:@"amount"]];
    }
    else if (indexPath.row==3)
    {
        cell.titleLabel.text= @"Location";
        cell.description_Label.text= [detailsListing.details_Listing valueForKey:@"location"];
    }
    
    else if (indexPath.row==4)
    {
        cell.titleLabel.text=@"";
        cell.description_Label.text=@"";
    }
   
    else
    {
    cell.textLabel.text=@"Hello....";
    cell.description_Label.text=@"Hello....";
    }
    
}



- (UIImage *) placeHolderImageForImagePager{
    return [UIImage imageNamed:@"no_image.jpg"];
}

- (void)configureView
{
    // Update the user interface for the detail vehicle, if it exists.
    if (self.detailsListing)
    {
        NSLog(@"Everything Fetched:%@",[detailsListing details_Listing]);
    }

    self.headerView.dataSource = self;
    self.headerView.backgroundColor = [UIColor clearColor];
    [self.DetailPageTableView addSubview:self.headerView];
    
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonBack.frame = CGRectMake(10, 10, 44, 44);
    [buttonBack setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [buttonBack addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buttonBack];
    
    
    self.DetailPageTableView.rowHeight = UITableViewAutomaticDimension;

}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self configureView];
}



#pragma mark - UITableView
- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    static DynamicTableViewCell *cell = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        cell = [self.DetailPageTableView dequeueReusableCellWithIdentifier:CellIdentifier];

    });
    
    [self setUpCell:cell atIndexPath:indexPath];
    
    return [self calculateHeightForConfiguredSizingCell:cell];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    DynamicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    [self setUpCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}



#pragma mark - LocationDetailViewDelegate

- (void)locationDetail:(TGFoursquareLocationDetail *)locationDetail imagePagerDidLoad:(KIImagePager *)imagePager
{
    imagePager.dataSource = self;
    imagePager.delegate = self;
    imagePager.pageControl.currentPageIndicatorTintColor = [UIColor lightGrayColor];
    imagePager.pageControl.pageIndicatorTintColor = [UIColor orangeColor];
    imagePager.slideshowTimeInterval = 0.0f;
    imagePager.slideshowShouldCallScrollToDelegate = YES;
    self.locationDetail.nbImages = [self.locationDetail.imagePager.dataSource.arrayWithImages count];
    self.locationDetail.currentImage = 0;
}


#pragma mark - KIImagePager DataSource
- (NSArray *) arrayWithImages
{
    return [detailsListing.details_Listing valueForKey:@"images"];
}


- (UIViewContentMode) contentModeForImage:(NSUInteger)image
{
    return UIViewContentModeScaleAspectFill;
}


//- (NSString *) captionForImageAtIndex:(NSUInteger)index
//{
//    return @[
//             @"First screenshot",
//             @"Another screenshot",
//             @"And another one",
//             @"Last one! ;-)"
//             ][index];
//}


#pragma mark - Button actions

- (void)back
{
    NSLog(@"Here you should go back to previous view controller");
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];

}

- (void)post
{
    NSLog(@"Post action");
}

#pragma mark-
#pragma make call

- (IBAction)makeCallTouchUpInside
{
    NSString * phoneNumber= [NSString stringWithFormat:@"tel:%@",[self.detailsListing.details_Listing valueForKey:@"phone"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];

    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:phoneNumber]])
        
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
    }
    
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..",@"message":@"Unable to make Call"}];
    }
}

-(IBAction)addReviewTouchUpInside
{

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
