//
//  categoryListingcell.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 11/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface categoryListingcell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *listingLabel;

@end
