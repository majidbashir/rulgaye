//
//  BrowseVC.h
//  RulGaye.pk
//
//  Created by Maajid Bashir on 01/10/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import "browseCell.h"
#import "NavigationHandler.h"
#import "CategoryManager.h"




@interface BrowseVC : NavigationHandler <UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate, UISearchDisplayDelegate>

{
    NSInteger _currentPage;
    NSInteger _totalPages;
    
    
    BOOL isChecked;
}


@property (weak, nonatomic) IBOutlet UIView * NotificationView;

@property(nonatomic, assign) NSInteger category_ID;

@property (weak, nonatomic) IBOutlet UITableView * listingTableView;
@property (nonatomic,strong) NSMutableArray * favourites;

@property IBOutlet UISearchBar * searchBar;

@property(nonatomic,assign) NSInteger search_item_id;

@property(nonatomic,assign) NSString *urlString;

@property (nonatomic, strong) NSMutableArray * added2FavArray;

@end
