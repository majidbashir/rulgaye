//
//  CreateScheduleCell.h
//  Smart Sales
//
//  Created by Maajid Bashir on 20/03/2015.
//  Copyright (c) 2015 Smart Intellect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreateScheduleCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *user_text_field;

@end
