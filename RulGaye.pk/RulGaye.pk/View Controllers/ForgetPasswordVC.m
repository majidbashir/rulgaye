//
//  ForgetPasswordVC.m
//  RulGaye.pk
//
//  Created by Maajid Bashir on 17/11/2015.
//  Copyright © 2015 Cozmuler. All rights reserved.
//

#import "ForgetPasswordVC.h"
#import <JTProgressHUD.h>


@implementation ForgetPasswordVC
@synthesize resetEmailField;

- (IBAction)resetPassTouchUpInside
{
    [self makeRequest];
}

- (IBAction)cancelButtonTouchUpInside
{
    [self dismissViewControllerAnimated:YES
                             completion:nil];
}

-(void)makeRequest
{
    if ([self.resetEmailField.text isEqualToString:@""])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":@"All Fields required"}];
    }
    else
    {
        @try
        {
            [JTProgressHUD show];
            AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
            NSDictionary * param = [NSDictionary dictionaryWithObjectsAndKeys:self.resetEmailField.text,@"user_id", nil];
            
            manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
            
            [manager POST:(kBaseUrls @"forgetPassword") parameters:param progress:nil success:^(NSURLSessionTask *operation, id responseObject)
             
            {
                 [JTProgressHUD hide];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":responseObject}];
             }
            
             
                  failure:^(NSURLSessionTask  *operation, NSError *error)
             {
                 [JTProgressHUD hide];
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":[[error userInfo]valueForKey:@"NSLocalizedDescription"]}];
             }];
        }
        
        @catch (NSException * exception)
        
        {
            [JTProgressHUD hide];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowAlert" object:nil userInfo:@{@"title":@"RulGaye..\n",@"message":exception}];
        }
    }
    
}

@end
